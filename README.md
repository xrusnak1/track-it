# Track It

Semestral project for PV239 Developement of mobile applications
<!-- 
![activities](images/activities.PNG)
![activities](images/activities.PNG)
![activities](images/activities.PNG) -->

<table>
  <tr>
    <td>Timer with the activities</td>
     <td>Projects overview</td>
     <td>Page with stats</td>
  </tr>
  <tr>
    <td><img src="images/activities.PNG" width=300></td>
    <td><img src="images/projects.PNG" width=300></td>
    <td><img src="images/stats.PNG" width=300></td>
  </tr>
 </table>

## Authors

- Miroslav Bezák
- David Rusnák

## Specification

https://docs.google.com/document/d/e/2PACX-1vSSMk7ZkkdtxD2cKbtLOCLlUJl6dBrtl5girkR0D4HnOAigDCBTLAcuxKw_lY4RcFyC9cw4xhgYy-i1/pub

## Data model changes

To keep data in Firestore database, data models like **Activity**, **Project**, **Team** need to be serialized into and from JSON. The classes doing this serialization are auto-generated. After any model change, these files need to be regenerated running this command from the project root
```
flutter pub run build_runner build --delete-conflicting-outputs
```