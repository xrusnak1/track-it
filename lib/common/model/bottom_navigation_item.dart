// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class BottomNavigationItem {
  final String label;
  final Icon icon;
  final Widget page;
  final Widget? appBarAction;

  const BottomNavigationItem({
    required this.label,
    required this.icon,
    required this.page,
    this.appBarAction
  });
}
