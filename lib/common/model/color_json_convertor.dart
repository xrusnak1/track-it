import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

class ColorJsonConvertor extends JsonConverter<Color, int> {
  const ColorJsonConvertor();

  @override
  Color fromJson(int colorNumber) {
    return Color(colorNumber).withOpacity(1);
  }

  @override
  int toJson(Color color) {
    return color.value;
  }

}