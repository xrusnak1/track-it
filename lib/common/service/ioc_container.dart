import 'package:get_it/get_it.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/stats/service/stats_service.dart';
import 'package:track_it/teams/service/team_service.dart';
import 'package:track_it/timer/service/activity_service.dart';
import 'package:track_it/timer/service/timer_service.dart';

class IoCContainer {
  void setup() {
    GetIt.I.registerSingleton(UserService());
    GetIt.I.registerSingleton(TimerService());
    GetIt.I.registerSingleton(ActivityService());
    GetIt.I.registerSingleton(TeamService());
    GetIt.I.registerSingleton(ProjectService());
    GetIt.I.registerSingleton(StatsService());
  }
}
