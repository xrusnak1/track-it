import 'package:firebase_auth/firebase_auth.dart';

class UserService {
  bool get isLoggedIn => FirebaseAuth.instance.currentUser != null;

  String? get loggedInUserId => FirebaseAuth.instance.currentUser?.email;

  Future<String?> register({
    required String email,
    required String password,
  }) async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      } else {
        return e.message;
      }
    } catch (e) {
      return e.toString();
    }
  }

  Future<String?> login({
    required String email,
    required String password,
  }) async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      } else {
        return e.message;
      }
    } catch (e) {
      return e.toString();
    }
  }

  Stream<User?> getUserChangesStream() {
    return FirebaseAuth.instance.userChanges();
  }

  Future<bool> isEmailRegistered(String emailAddress) async {
    try {
      final list =
            await FirebaseAuth.instance.fetchSignInMethodsForEmail(emailAddress);
      return list.isNotEmpty;
    } catch (error) {
      return false;
    }
  }
}
