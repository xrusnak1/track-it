import 'dart:math';

class Utils {
  static String getDurationFormattedString(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
}

extension StringTrimmer on String {
  String shorten(int length) {
    if (length < this.length) {
      return '${substring(0, min(length, this.length))}...';
    }

    return this;
  }
}
