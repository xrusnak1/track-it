import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/common/widget/login_page.dart';

import 'bottom_navigator.dart';

class AppWrapper extends StatelessWidget {
  const AppWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TrackIt',
      theme: ThemeData(
          primarySwatch: Colors.purple,
          textTheme: Theme.of(context).textTheme.apply(
                fontSizeFactor: 1.1,
                fontSizeDelta: 2.0,
              )),
      // Only logged in users are allowed to use the app
      home: _buildLoginOrMainPage(),
    );
  }

  Widget _buildLoginOrMainPage() {
    //FirebaseAuth.instance.signOut();
    final userService = GetIt.I.get<UserService>();
    return StreamBuilder<User?>(
      stream: userService.getUserChangesStream(),
      builder: (context, AsyncSnapshot<User?> snapshot) {
        print("user: ${snapshot.data?.email ?? "null"}");
        if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error!}'));
        }
        if (!snapshot.hasData || snapshot.data == null) {
          return LoginPage();
        } else {
          return BottomNavigator();
        }
      },
    );
  }
}
