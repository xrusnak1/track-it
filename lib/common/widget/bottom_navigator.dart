import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:track_it/common/model/bottom_navigation_item.dart';
import 'package:track_it/projects/widget/create_or_edit_project_page.dart';
import 'package:track_it/projects/widget/project_page.dart';
import 'package:track_it/stats/widget/filter_page.dart';
import 'package:track_it/stats/widget/stats_page.dart';
import 'package:track_it/teams/widget/create_or_edit_team_page.dart';
import 'package:track_it/teams/widget/teams_page.dart';
import 'package:track_it/timer/widget/timer_page.dart';

class BottomNavigator extends StatefulWidget {
  const BottomNavigator({super.key});

  @override
  State<BottomNavigator> createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final List<BottomNavigationItem> bottomNavigationItems = [
      BottomNavigationItem(
          label: 'Timer',
          icon: Icon(Icons.timelapse),
          page: TimerPage(),
          appBarAction: IconButton(
            onPressed: () {
              FirebaseAuth.instance.signOut();
            },
            icon: Icon(Icons.logout),
          )),
      BottomNavigationItem(
        label: 'My Teams',
        icon: Icon(Icons.group),
        page: TeamsPage(),
        appBarAction: IconButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => CreateOrEditTeamPage(),
              ),
            );
          },
          icon: Icon(Icons.add),
        ),
      ),
      BottomNavigationItem(
        label: 'Projects',
        icon: Icon(Icons.lightbulb),
        page: ProjectPage(),
        appBarAction: IconButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => CreateOrEditProjectPage(),
              ),
            );
          },
          icon: Icon(Icons.add),
        ),
      ),
      BottomNavigationItem(
        label: 'Stats',
        icon: Icon(Icons.query_stats),
        page: StatsPage(),
        appBarAction: IconButton(
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => FilterPage(),
              ),
            );
          },
          icon: Icon(Icons.filter_list_alt),
        ),
      ),
    ];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(bottomNavigationItems[_selectedIndex].label),
        actions: bottomNavigationItems[_selectedIndex].appBarAction == null
            ? []
            : [bottomNavigationItems[_selectedIndex].appBarAction!],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: bottomNavigationItems
            .map((item) =>
                BottomNavigationBarItem(label: item.label, icon: item.icon))
            .toList(),
        currentIndex: _selectedIndex,
        onTap: _onNavigationItemTapped,
      ),
      body: bottomNavigationItems.elementAt(_selectedIndex).page,
    );
  }

  void _onNavigationItemTapped(int index) =>
      setState(() => _selectedIndex = index);
}
