import 'package:flutter/cupertino.dart';

class StreamBuilderWrapper<T> extends StatelessWidget {
  final Stream<T> stream;
  final Widget Function(BuildContext context, T data) builder;

  const StreamBuilderWrapper(
      {super.key, required this.stream, required this.builder});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      stream: stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error!}'));
        }

        if (!snapshot.hasData) {
          return Center(child: CupertinoActivityIndicator());
        }

        return builder(context, snapshot.data!);
      },
    );
  }
}
