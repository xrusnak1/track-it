import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:track_it/common/widget/app_wrapper.dart';
import 'package:track_it/firebase_options.dart';

import 'common/service/ioc_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  IoCContainer().setup();
  runApp(const AppWrapper());
}
