import 'package:flutter/material.dart';
import 'package:track_it/common/model/color_json_convertor.dart';
import 'package:uuid/uuid.dart';
import 'package:json_annotation/json_annotation.dart';

part 'project.g.dart';

@JsonSerializable()
class Project {
  final String? id;
  final String name;
  @ColorJsonConvertor()
  final Color color;
  final String ownerId; // references User.id
  final String teamId; // references Team.id

  Project(
      {required this.id,
      required this.name,
      required this.color,
      required this.ownerId,
      required this.teamId});

  Project.generatedId({
    required this.name,
    required this.color,
    required this.ownerId,
    required this.teamId,
  }) : id = Uuid().v4();

  Project copyWith({String? id, String? name, Color? color, String? ownerId, String? teamId}) {
    return Project(
      id: id ?? this.id,
      name: name ?? this.name,
      color: color ?? this.color,
      ownerId: ownerId ?? this.ownerId,
      teamId: teamId ?? this.teamId
    );
  }

  factory Project.fromJson(Map<String, dynamic> json) =>
      _$ProjectFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectToJson(this);

  @override
  String toString() =>
      'Project {name: $name, color: $color, ownerId: $ownerId}';
}
