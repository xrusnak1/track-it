// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Project _$ProjectFromJson(Map<String, dynamic> json) => Project(
      id: json['id'] as String?,
      name: json['name'] as String,
      color: const ColorJsonConvertor().fromJson(json['color'] as int),
      ownerId: json['ownerId'] as String,
      teamId: json['teamId'] as String,
    );

Map<String, dynamic> _$ProjectToJson(Project instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'color': const ColorJsonConvertor().toJson(instance.color),
      'ownerId': instance.ownerId,
      'teamId': instance.teamId,
    };
