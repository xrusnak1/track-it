import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/teams/service/team_service.dart';

const PROJECTS_COLLECTION = 'projects';

class ProjectService {
  final _userService = GetIt.I<UserService>();
  final _teamService = GetIt.I<TeamService>();

  void add(Project project) {
    _checkLogin();

    final newProject = project.copyWith(ownerId: _userService.loggedInUserId!);
    update(newProject);
  }

  void update(Project project) {
    _checkLogin();

    FirebaseFirestore.instance
        .collection(PROJECTS_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Project.fromJson(snap.data()!),
            toFirestore: (project, _) => project.toJson())
        .doc(project.id)
        .set(project)
        .then((_) {},
            onError: (e) => print('Can not add project ${project.id} $e'));
  }

  void remove(String projectId) {
    _checkLogin();

    FirebaseFirestore.instance
        .collection(PROJECTS_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Project.fromJson(snap.data()!),
            toFirestore: (project, _) => project.toJson())
        .doc(projectId)
        .delete()
        .then((_) {},
            onError: (e) => print('Failed to delete project $projectId [$e}]'));
  }

  Stream<List<Project>> projectsStream() {
    _checkLogin();

    final allProjects = FirebaseFirestore.instance
        .collection(PROJECTS_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Project.fromJson(snap.data()!),
            toFirestore: (project, _) => project.toJson())
        .snapshots()
        .map((QuerySnapshot<Project> snap) =>
            snap.docs.map((doc) => doc.data()).toList());

    return Rx.combineLatest2(allProjects, _teamService.teamsStream(),
        (projects, teams) {
      final myTeams = teams
          .where((t) => t.membersIds.contains(_userService.loggedInUserId!))
          .toList()
          .map((t) => t.id!);
      return projects.where((p) => myTeams.contains(p.teamId)).toList();
    });
  }

  void _checkLogin() {
    if (!_userService.isLoggedIn) {
      throw StateError('Can not perform action while not logged in');
    }
  }
}
