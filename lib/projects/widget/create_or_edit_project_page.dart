import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/teams/model/team.dart';
import 'package:track_it/teams/service/team_service.dart';

const COLOR_BOX_SIZE = 40.0;
const DEFAULT_COLOR = Color(0xff443a49);

class CreateOrEditProjectPage extends StatefulWidget {
  final Project? projectToUpdate;

  const CreateOrEditProjectPage({super.key, this.projectToUpdate});

  @override
  State<CreateOrEditProjectPage> createState() =>
      _CreateOrEditProjectPageState();
}

class _CreateOrEditProjectPageState extends State<CreateOrEditProjectPage> {
  final _newProjectEditingController = TextEditingController();

  Color _pickerColor = DEFAULT_COLOR;
  Color _selectedColor = DEFAULT_COLOR;
  String? _teamId;

  void changeColor(Color color) {
    setState(() => _pickerColor = color);
  }

  @override
  void initState() {
    super.initState();
    if (widget.projectToUpdate != null) {
      _newProjectEditingController.text = widget.projectToUpdate!.name;
      _selectedColor = widget.projectToUpdate!.color;
      _teamId = widget.projectToUpdate!.teamId;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.projectToUpdate == null
            ? 'New Project'
            : 'Update ${widget.projectToUpdate?.name}'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextField(
              controller: _newProjectEditingController,
              decoration: const InputDecoration(hintText: 'Name'),
              autocorrect: false,
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Color', style: TextStyle(fontSize: 20.0)),
                GestureDetector(
                  onTap: _displayColorPicker,
                  child: Container(
                    height: COLOR_BOX_SIZE,
                    width: COLOR_BOX_SIZE,
                    color: _selectedColor,
                  ),
                )
              ],
            ),
            SizedBox(height: 20),
            _buildTeamSelector(),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _onProjectSubmitted,
              child: Text(widget.projectToUpdate == null ? 'Create' : 'Update'),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTeamSelector() {
    final teamService = GetIt.I<TeamService>();
    return StreamBuilderWrapper<List<Team>>(
        stream: teamService.teamsStream(),
        builder: (context, teams) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Team', style: TextStyle(fontSize: 20.0)),
              DropdownButton(
                value: _teamId,
                icon: const Icon(Icons.keyboard_arrow_down),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                items: teams
                    .map((t) => DropdownMenuItem(
                          value: t.id,
                          child: Text(t.name),
                        ))
                    .toList(),
                onChanged: (String? teamId) {
                  setState(() {
                    _teamId = teamId;
                  });
                },
              )
            ],
          );
        });
  }

  void _onProjectSubmitted() {
    final name = _newProjectEditingController.text.trim();

    final projectService = GetIt.I<ProjectService>();
    if (widget.projectToUpdate == null) {
      projectService.add(
        Project.generatedId(
          name: name,
          color: _selectedColor,
          ownerId: 'replaced in service',
          teamId: _teamId!,
        ),
      );
    } else {
      final updatedProject = widget.projectToUpdate!
          .copyWith(name: name, color: _selectedColor, teamId: _teamId);

      projectService.update(updatedProject);
    }

    Navigator.of(context).pop();
  }

  void _displayColorPicker() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('Pick a color!'),
              content: SingleChildScrollView(
                child: BlockPicker(
                  pickerColor: _selectedColor,
                  onColorChanged: changeColor,
                ),
              ),
              actions: [
                ElevatedButton(
                  child: const Text('Got it'),
                  onPressed: () {
                    setState(() => _selectedColor = _pickerColor);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }
}
