import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/projects/widget/project_tile.dart';

class ProjectPage extends StatelessWidget {
  const ProjectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final projectService = GetIt.I<ProjectService>();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilderWrapper(
          stream: projectService.projectsStream(),
          builder: (BuildContext context, List<Project> projects) {
            return ListView.separated(
                itemBuilder: (_, i) {
                  return ProjectTile(
                    project: projects[i],
                  );
                },
                separatorBuilder: (_, __) => SizedBox(height: 10),
                itemCount: projects.length);
          }),
    );
  }
}
