import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/projects/widget/create_or_edit_project_page.dart';

class ProjectTile extends StatelessWidget {
  final Project project;

  const ProjectTile({
    super.key,
    required this.project,
  });

  @override
  Widget build(BuildContext context) {
    final userService = GetIt.I<UserService>();

    return Container(
      padding: EdgeInsets.all(3),
      // height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: project.color,
      ),
      child: ListTile(
        title: Text(project.name),
        trailing: project.ownerId == userService.loggedInUserId
            ? IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CreateOrEditProjectPage(
                        projectToUpdate: project,
                      ),
                    ),
                  );
                },
                icon: Icon(Icons.edit),
              )
            : null,
      ),
    );
  }
}
