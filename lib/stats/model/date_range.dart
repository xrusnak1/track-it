import 'package:basic_utils/basic_utils.dart';

enum DateRange { infinite, today, lastWeek, lastMonth }

extension DateRangeToString on DateRange {
  String readableToString() {
    final withUnderScores = StringUtils.camelCaseToLowerUnderscore(name);
    final capitalized = StringUtils.capitalize(withUnderScores);
    return capitalized.replaceAll('_', ' ');
  }
}
