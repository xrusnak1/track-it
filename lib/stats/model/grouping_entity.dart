import 'package:basic_utils/basic_utils.dart';

enum GroupingEntity { project, team }

extension DateRangeToString on GroupingEntity {
  String readableToString() => StringUtils.capitalize(name);
}
