import 'package:flutter/material.dart';
import 'package:track_it/timer/model/activity.dart';

class StatsEntry {
  final Activity activity;
  final String name;
  final Color color;

  StatsEntry({required this.activity, required this.name, required this.color});
}
