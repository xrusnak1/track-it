import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/stats/model/date_range.dart';
import 'package:track_it/stats/model/grouping_entity.dart';
import 'package:track_it/stats/model/stats_entry.dart';
import 'package:track_it/teams/model/team.dart';
import 'package:track_it/teams/service/team_service.dart';
import 'package:track_it/timer/model/activity.dart';
import 'package:track_it/timer/service/activity_service.dart';

import 'package:rxdart/rxdart.dart';

class StatsService {
  final _projectService = GetIt.I<ProjectService>();
  final _activityService = GetIt.I<ActivityService>();
  final _teamService = GetIt.I<TeamService>();
  final _dateRangeStream = BehaviorSubject<DateRange>();
  final _groupingEntityStream = BehaviorSubject<GroupingEntity>();

  StatsService() {
    _dateRangeStream.add(DateRange.infinite);
    _groupingEntityStream.add(GroupingEntity.project);
  }

  void setDateRange(DateRange dateRange) {
    _dateRangeStream.add(dateRange);
  }

  void setGroupingEntity(GroupingEntity groupingEntity) {
    _groupingEntityStream.add(groupingEntity);
  }

  Stream<DateRange> get dateRangeStream => _dateRangeStream;

  Stream<GroupingEntity> get groupingEntityStream => _groupingEntityStream;

  Stream<List<StatsEntry>> get statsStream => Rx.combineLatest5(
      _projectService.projectsStream(),
      _activityService.activitiesStream(),
      _teamService.teamsStream(),
      _dateRangeStream,
      _groupingEntityStream,
      (projects, activities, teams, range, grouping) => activities
          .filterByDateRange(range)
          .map(
            (a) => StatsEntry(
              activity: a,
              name: _findName(a, projects, teams, grouping),
              color: _findColor(a, projects, teams, grouping),
            ),
          )
          .toList());

  String _findName(Activity activity, List<Project> projects, List<Team> teams,
      GroupingEntity grouping) {
    if (grouping == GroupingEntity.project) {
      final project =
          projects.firstWhereOrNull((p) => p.id == activity.projectId);
      return project != null ? project.name : "No project";
    } else {
      final project =
          projects.firstWhereOrNull((p) => p.id == activity.projectId);
      if (project == null) {
        return "No team";
      }
      return teams.firstWhere((t) => t.id == project.teamId).name;
    }
  }

  Color _findColor(Activity activity, List<Project> projects, List<Team> teams,
      GroupingEntity grouping) {
    if (grouping == GroupingEntity.project) {
      final project =
          projects.firstWhereOrNull((p) => p.id == activity.projectId);
      return project != null ? project.color : Colors.red.shade300;
    } else {
      final project =
          projects.firstWhereOrNull((p) => p.id == activity.projectId);
      if (project == null) {
        return Colors.red.shade300;
      }
      return teams.firstWhere((t) => t.id == project.teamId).color;
    }
  }
}

extension ActivityFilter on List<Activity> {
  List<Activity> filterByDateRange(DateRange dateRange) {
    switch (dateRange) {
      case DateRange.infinite:
        return this;
      case DateRange.today:
        final now = DateTime.now();
        final lastMidnight = DateTime(now.year, now.month, now.day);
        return where((a) => a.begin.isAfter(lastMidnight)).toList();
      case DateRange.lastWeek:
        final oneWeekAgo = DateTime.now().subtract(Duration(days: 7));
        return where((a) => a.begin.isAfter(oneWeekAgo)).toList();
      case DateRange.lastMonth:
        final oneMonthAgo = DateTime.now().subtract(Duration(days: 30));
        return where((a) => a.begin.isAfter(oneMonthAgo)).toList();
    }
  }
}
