import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/stats/model/date_range.dart';
import 'package:track_it/stats/model/grouping_entity.dart';
import 'package:track_it/stats/service/stats_service.dart';

class FilterPage extends StatelessWidget {
  const FilterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Activity Filters'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Date Range:', style: TextStyle(fontSize: 20.0)),
                _buildTimeFilter(),
                SizedBox(),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Group by:', style: TextStyle(fontSize: 20.0)),
                _buildGroupingFilter(),
                SizedBox(),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTimeFilter() {
    final statsService = GetIt.I.get<StatsService>();
    return StreamBuilderWrapper(
        stream: statsService.dateRangeStream,
        builder: (context, dateRange) => DropdownButton<DateRange>(
            value: dateRange,
            icon: const Icon(Icons.keyboard_arrow_down),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            items: DateRange.values
                .map((e) => DropdownMenuItem<DateRange>(
                      value: e,
                      child: Text(e.readableToString()),
                    ))
                .toList(),
            onChanged: (value) =>
                statsService.setDateRange(value ?? DateRange.infinite)));
  }

  Widget _buildGroupingFilter() {
    final statsService = GetIt.I.get<StatsService>();
    return StreamBuilderWrapper(
        stream: statsService.groupingEntityStream,
        builder: (context, groupingEntity) => DropdownButton<GroupingEntity>(
            value: groupingEntity,
            icon: const Icon(Icons.keyboard_arrow_down),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            items: GroupingEntity.values
                .map((e) => DropdownMenuItem<GroupingEntity>(
                      value: e,
                      child: Text(e.readableToString()),
                    ))
                .toList(),
            onChanged: (value) => statsService
                .setGroupingEntity(value ?? GroupingEntity.project)));
  }
}
