import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/stats/model/stats_entry.dart';
import 'package:track_it/stats/service/stats_service.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:pie_chart/pie_chart.dart';

class StatsPage extends StatelessWidget {
  const StatsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final statsService = GetIt.I.get<StatsService>();
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: StreamBuilderWrapper(
          stream: statsService.statsStream,
          builder: (BuildContext context, List<StatsEntry> stats) {
            return stats.isEmpty
                ? Center(child: Text('No activities for set filters'))
                : Column(
                    children: [
                      _buildTotalLabel(stats),
                      SizedBox(height: 10),
                      _buildPieChart(stats),
                      SizedBox(height: 10),
                      _buildAverageLabel(stats),
                      SizedBox(height: 10),
                      Expanded(child: _buildBarChart(stats)),
                      SizedBox(height: 10),
                      _buildShortestLabel(stats),
                      SizedBox(height: 10),
                      _buildLongestLabel(stats),
                    ],
                  );
          }),
    );
  }

  Widget _buildTotalLabel(List<StatsEntry> stats) {
    String total = stats
        .map((s) => s.activity.duration)
        .reduce((value, element) => value + element)
        .format();
    return _buildStatLabel('Total', total);
  }

  Widget _buildPieChart(List<StatsEntry> stats) {
    Map<String, double> dataMap =
        groupBy(stats, (StatsEntry stat) => stat.name).map(
      (projectName, stats) => MapEntry(
          projectName,
          stats
              .map((s) => s.activity.duration.inSeconds)
              .reduce((value, element) => value + element)
              .toDouble()),
    );
    return PieChart(
      dataMap: dataMap,
      colorList: stats.map((s) => s.color).toList(),
      chartValuesOptions: ChartValuesOptions(
        decimalPlaces: 0,
        showChartValuesInPercentage: true,
      ),
    );
  }

  Widget _buildAverageLabel(List<StatsEntry> stats) {
    final averageInSeconds =
        stats.map((s) => s.activity.duration.inSeconds).average.round();
    final average = Duration(seconds: averageInSeconds).format();

    return _buildStatLabel('Average', average);
  }

  Widget _buildBarChart(List<StatsEntry> stats) {
    final seriesList = [
      charts.Series<StatsEntry, String>(
        id: 'Stats',
        colorFn: (statEntry, _) =>
            charts.ColorUtil.fromDartColor(statEntry.color),
        domainFn: (statEntry, _) => statEntry.activity.title,
        measureFn: (statEntry, _) => statEntry.activity.duration.inSeconds,
        data: stats,
        labelAccessorFn: (statEntry, _) => statEntry.activity.duration.format(),
      )
    ];
    return charts.BarChart(
      seriesList,
      animate: true,
      animationDuration: Duration(seconds:1),
    );
  }

  Widget _buildShortestLabel(List<StatsEntry> stats) {
    final shortestInSeconds =
        stats.map((s) => s.activity.duration.inSeconds).min;
    final min = Duration(seconds: shortestInSeconds).format();

    return _buildStatLabel('Shortest', min);
  }

  Widget _buildLongestLabel(List<StatsEntry> stats) {
    final longestInSeconds =
        stats.map((s) => s.activity.duration.inSeconds).max;
    final longest = Duration(seconds: longestInSeconds).format();

    return _buildStatLabel('Longest', longest);
  }

  Widget _buildStatLabel(String label, String duration) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(width: 1),
        Text('$label:', style: TextStyle(fontSize: 20)),
        Text(duration, style: TextStyle(fontSize: 20)),
        SizedBox(width: 1),
      ],
    );
  }
}

extension on Duration {
  String format() => '$this'.split('.')[0].padLeft(8, '0');
}
