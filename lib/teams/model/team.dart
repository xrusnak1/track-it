import 'package:flutter/material.dart';
import 'package:track_it/common/model/color_json_convertor.dart';
import 'package:uuid/uuid.dart';
import 'package:json_annotation/json_annotation.dart';

part 'team.g.dart';

@JsonSerializable()
class Team {
  final String? id;
  final String name;
  @ColorJsonConvertor()
  final Color color;
  final String ownerId; //ref to User.userId
  final List<String> membersIds;

  Team(
      {required this.id,
      required this.name,
      required this.color,
      required this.ownerId,
      required this.membersIds});

  Team.generatedId(
      {required this.name,
      required this.color,
      required this.ownerId,
      required this.membersIds})
      : id = Uuid().v4();

  Team copyWith(
      {String? id,
      String? name,
      Color? color,
      String? ownerId,
      List<String>? membersIds}) {
    return Team(
        id: id ?? this.id,
        name: name ?? this.name,
        color: color ?? this.color,
        ownerId: ownerId ?? this.ownerId,
        membersIds: membersIds ?? this.membersIds);
  }

  factory Team.fromJson(Map<String, dynamic> json) => _$TeamFromJson(json);

  Map<String, dynamic> toJson() => _$TeamToJson(this);
  @override
  String toString() =>
      'Team {name: $name, color: $color, ownerId: $ownerId, membersIds: $membersIds}';
}
