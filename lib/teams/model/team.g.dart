// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Team _$TeamFromJson(Map<String, dynamic> json) => Team(
      id: json['id'] as String?,
      name: json['name'] as String,
      color: const ColorJsonConvertor().fromJson(json['color'] as int),
      ownerId: json['ownerId'] as String,
      membersIds: (json['membersIds'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$TeamToJson(Team instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'color': const ColorJsonConvertor().toJson(instance.color),
      'ownerId': instance.ownerId,
      'membersIds': instance.membersIds,
    };
