import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/teams/model/team.dart';

import '../../common/service/user_service.dart';

const TEAMS_COLLECTION = 'teams';

class TeamService {
  final _userService = GetIt.I<UserService>();

  void add(Team team) {
    _checkLogin();

    final newTeam = team.copyWith(ownerId: _userService.loggedInUserId!);
    update(newTeam);
  }

  void update(Team team) {
    _checkLogin();

    FirebaseFirestore.instance
        .collection(TEAMS_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Team.fromJson(snap.data()!),
            toFirestore: (activity, _) => activity.toJson())
        .doc(team.id)
        .set(team)
        .then((_) {}, onError: (e) => print('Can not add team ${team.id} $e'));
  }

  void remove(String teamId) {
    _checkLogin();

    FirebaseFirestore.instance
        .collection(TEAMS_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Team.fromJson(snap.data()!),
            toFirestore: (team, _) => team.toJson())
        .doc(teamId)
        .delete()
        .then((_) {},
            onError: (e) => print('Failed to delete team $teamId [$e}]'));
  }

  Stream<List<Team>> teamsStream() {
    _checkLogin();

    return FirebaseFirestore.instance
        .collection(TEAMS_COLLECTION)
        .where('membersIds', arrayContains: _userService.loggedInUserId!)
        .withConverter(
            fromFirestore: (snap, _) => Team.fromJson(snap.data()!),
            toFirestore: (activity, _) => activity.toJson())
        .snapshots()
        .map((QuerySnapshot<Team> snap) =>
            snap.docs.map((doc) => doc.data()).toList());
  }

  void _checkLogin() {
    if (!_userService.isLoggedIn) {
      throw StateError('Can not perform action while not logged in');
    }
  }
}
