import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/teams/model/team.dart';
import 'package:track_it/teams/service/team_service.dart';
import 'package:track_it/teams/widget/new_team_member_page.dart';

const COLOR_BOX_SIZE = 40.0;
const DEFAULT_COLOR = Color(0xff443a49);

class CreateOrEditTeamPage extends StatefulWidget {
  final Team? teamToUpdate;

  const CreateOrEditTeamPage({super.key, this.teamToUpdate});

  @override
  State<CreateOrEditTeamPage> createState() => _CreateOrEditTeamPageState();
}

class _CreateOrEditTeamPageState extends State<CreateOrEditTeamPage> {
  final _newTeamEditingController = TextEditingController();

  Color _pickerColor = DEFAULT_COLOR;
  Color _selectedColor = DEFAULT_COLOR;

  void changeColor(Color color) {
    setState(() => _pickerColor = color);
  }

  List<String> _memberIds = [];
  String? _ownerId;

  @override
  void initState() {
    super.initState();
    if (widget.teamToUpdate != null) {
      _newTeamEditingController.text = widget.teamToUpdate!.name;
      _selectedColor = widget.teamToUpdate!.color;
      _memberIds = widget.teamToUpdate!.membersIds;
      _ownerId = widget.teamToUpdate!.ownerId;
    } else {
      final userService = GetIt.I<UserService>();
      _memberIds = [userService.loggedInUserId!];
      _ownerId = userService.loggedInUserId!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.teamToUpdate == null
            ? 'New Team'
            : 'Update ${widget.teamToUpdate?.name}'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextField(
              controller: _newTeamEditingController,
              decoration: const InputDecoration(hintText: 'Name'),
              autocorrect: false,
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Color', style: TextStyle(fontSize: 20.0)),
                GestureDetector(
                  onTap: _displayColorPicker,
                  child: Container(
                    height: COLOR_BOX_SIZE,
                    width: COLOR_BOX_SIZE,
                    color: _selectedColor,
                  ),
                )
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Members', style: TextStyle(fontSize: 20.0)),
                IconButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => NewTeamMemberPage(
                          onMemberAdded: (String newMember) {
                            setState(() {
                              _memberIds.add(newMember);
                            });
                          },
                        ),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.add,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                )
              ],
            ),
            _buildMembersList(),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _onTeamSubmitted,
              child: Text(widget.teamToUpdate == null ? 'Create' : 'Update'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMembersList() {
    return Expanded(
      child: ListView.separated(
        itemBuilder: (_, i) {
          return ListTile(
            key: ValueKey(i),
            title: Text(_memberIds[i]),
            trailing: _memberIds[i] != _ownerId
                ? IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      setState(() {
                        _memberIds.removeAt(i);
                      });
                    },
                  )
                : null,
          );
        },
        separatorBuilder: (_, __) => Divider(),
        itemCount: _memberIds.length,
      ),
    );
  }

  void _onTeamSubmitted() {
    final name = _newTeamEditingController.text.trim();

    final teamService = GetIt.I<TeamService>();
    if (widget.teamToUpdate == null) {
      teamService.add(
        Team.generatedId(
            name: name,
            color: _selectedColor,
            ownerId: 'replaced in service',
            membersIds: _memberIds),
      );
    } else {
      final updatedTeam = widget.teamToUpdate!
          .copyWith(name: name, color: _selectedColor, membersIds: _memberIds);

      teamService.update(updatedTeam);
    }

    Navigator.of(context).pop();
  }

  void _displayColorPicker() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Pick a color!'),
        content: SingleChildScrollView(
          child: BlockPicker(
            pickerColor: _selectedColor,
            onColorChanged: changeColor,
          ),
        ),
        actions: [
          ElevatedButton(
            child: const Text('Got it'),
            onPressed: () {
              setState(() => _selectedColor = _pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
