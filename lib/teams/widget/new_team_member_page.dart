import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/user_service.dart';

class NewTeamMemberPage extends StatefulWidget {
  final Function onMemberAdded;

  const NewTeamMemberPage({required this.onMemberAdded, Key? key})
      : super(key: key);

  @override
  State<NewTeamMemberPage> createState() => _NewTeamMemberPageState();
}

class _NewTeamMemberPageState extends State<NewTeamMemberPage> {
  final _newTeamMemberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Team Member'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextField(
              controller: _newTeamMemberController,
              decoration:
                  const InputDecoration(hintText: 'Enter new team member'),
              autocorrect: false,
              onSubmitted: _addNewMember,
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () => _addNewMember(_newTeamMemberController.text),
              child: Text('Add'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _addNewMember(String newMember) async {
    final userService = GetIt.I<UserService>();
    if (await userService.isEmailRegistered(newMember)) {
      widget.onMemberAdded(newMember.trim());
      Navigator.of(context).pop();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('User with that email not found!'),
        ),
      );
    }
  }
}
