import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/user_service.dart';
import 'package:track_it/teams/model/team.dart';
import 'package:track_it/teams/widget/create_or_edit_team_page.dart';

class TeamTile extends StatelessWidget {
  final Team team;

  const TeamTile({
    super.key,
    required this.team,
  });

  @override
  Widget build(BuildContext context) {
    final userService = GetIt.I<UserService>();
    return Container(
      padding: EdgeInsets.all(3),
      // height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: team.color,
      ),
      child: ListTile(
        title: Text(team.name),
        trailing: team.ownerId == userService.loggedInUserId
            ? IconButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CreateOrEditTeamPage(
                        teamToUpdate: team,
                      ),
                    ),
                  );
                },
                icon: Icon(Icons.edit),
              )
            : null,
      ),
    );
  }
}
