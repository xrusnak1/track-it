import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/teams/model/team.dart';
import 'package:track_it/teams/service/team_service.dart';
import 'package:track_it/teams/widget/team_tile.dart';

class TeamsPage extends StatelessWidget {
  const TeamsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final teamService = GetIt.I<TeamService>();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilderWrapper(
          stream: teamService.teamsStream(),
          builder: (BuildContext context, List<Team> teams) {
            return ListView.separated(
                itemBuilder: (_, i) {
                  return TeamTile(
                    team: teams[i],
                  );
                },
                separatorBuilder: (_, __) => SizedBox(height: 10),
                itemCount: teams.length);
          }),
    );
  }
}
