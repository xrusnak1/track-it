import 'package:uuid/uuid.dart';
import 'package:json_annotation/json_annotation.dart';

part 'activity.g.dart';

@JsonSerializable()
class Activity {
  final String id;
  final DateTime begin;
  final DateTime end;
  final String title;
  final String? projectId;
  final String? userId;

  Activity(
      this.id, this.begin, this.end, this.title, this.projectId, this.userId) {
    if (begin.isAfter(end)) {
      throw Exception('begin must be before the end of activity');
    }
  }

  Activity.generatedId(
      this.begin, this.end, this.title, this.projectId, this.userId)
      : id = Uuid().v4();

  Activity copyWith(
      {String? id,
      DateTime? begin,
      DateTime? end,
      String? title,
      String? projectId,
      String? userId}) {
    return Activity(
      id ?? this.id,
      begin ?? this.begin,
      end ?? this.end,
      title ?? this.title,
      projectId ?? this.projectId,
      userId ?? this.userId,
    );
  }

  Activity updateTo(Activity newA) {
    return copyWith(
        begin: newA.begin,
        end: newA.end,
        title: newA.title,
        projectId: newA.projectId);
  }

  Duration get duration => end.difference(begin);

  factory Activity.fromJson(Map<String, dynamic> json) =>
      _$ActivityFromJson(json);

  Map<String, dynamic> toJson() => _$ActivityToJson(this);

  @override
  String toString() {
    return 'Activity{id: $id, begin: $begin, end: $end, title: $title, project: $projectId, userId: $userId}';
  }
}
