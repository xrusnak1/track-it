// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Activity _$ActivityFromJson(Map<String, dynamic> json) => Activity(
      json['id'] as String,
      DateTime.parse(json['begin'] as String),
      DateTime.parse(json['end'] as String),
      json['title'] as String,
      json['projectId'] as String?,
      json['userId'] as String?,
    );

Map<String, dynamic> _$ActivityToJson(Activity instance) => <String, dynamic>{
      'id': instance.id,
      'begin': instance.begin.toIso8601String(),
      'end': instance.end.toIso8601String(),
      'title': instance.title,
      'projectId': instance.projectId,
      'userId': instance.userId,
    };
