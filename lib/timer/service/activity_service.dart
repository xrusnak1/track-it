import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_it/get_it.dart';

import '../../common/service/user_service.dart';
import '../model/activity.dart';

const ACTIVITIES_COLLECTION = 'activities';

class ActivityService {
  final _userService = GetIt.I<UserService>();

  void add(Activity activity) {
    _checkLogin();

    final activityToAdd =
        activity.copyWith(userId: _userService.loggedInUserId!);
    update(activityToAdd);
  }

  void remove(String activityId) {
    _checkLogin();

    FirebaseFirestore.instance
        .collection(ACTIVITIES_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Activity.fromJson(snap.data()!),
            toFirestore: (activity, _) => activity.toJson())
        .doc(activityId)
        .delete()
        .then((_) {},
            onError: (e) =>
                print('Failed to delete activity $activityId [$e}]'));
  }

  Stream<List<Activity>> activitiesStream() {
    _checkLogin();

    return FirebaseFirestore.instance
        .collection(ACTIVITIES_COLLECTION)
        .where('userId', isEqualTo: _userService.loggedInUserId)
        .withConverter(
            fromFirestore: (snap, _) => Activity.fromJson(snap.data()!),
            toFirestore: (activity, _) => activity.toJson())
        .snapshots()
        .map((QuerySnapshot<Activity> snap) =>
            snap.docs.map((doc) => doc.data()).toList());
  }

  void update(Activity activity) {
    FirebaseFirestore.instance
        .collection(ACTIVITIES_COLLECTION)
        .withConverter(
            fromFirestore: (snap, _) => Activity.fromJson(snap.data()!),
            toFirestore: (activity, _) => activity.toJson())
        .doc(activity.id) //use activityId as doc ID
        .set(activity)
        .then((_) => {},
            onError: (e) => print('Can not add activity ${activity.id} $e'));
  }

  void _checkLogin() {
    if (!_userService.isLoggedIn) {
      throw StateError('Can not perform action while not logged in');
    }
  }
}
