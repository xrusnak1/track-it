import 'dart:async';

import 'package:rxdart/rxdart.dart';

class TimerService {
  DateTime? _begin;
  DateTime? _end;
  Timer? _timer;
  final _timerStream = BehaviorSubject<Duration>();

  TimerService() {
    _timerStream.add(Duration(seconds: 0));
  }

  bool get isRunning => _timer != null && _timer!.isActive;

  void _refreshTimerStream(_) {
    if (_begin != null) {
      _timerStream.add(_begin!.difference(DateTime.now()).abs());
    }
  }

  DateTime? get endDateTime => _end;

  DateTime? get beginDateTime => _begin;

  void _start() {
    if (!isRunning) {
      _begin = DateTime.now();
      _end = null;
      _timer = Timer.periodic(const Duration(seconds: 1), _refreshTimerStream);
    }
  }

  void _stop() {
    if (isRunning) {
      _timer!.cancel();
      _timer = null;
      _end = DateTime.now();
      _timerStream.add(Duration(seconds: 0));
    }
  }

  void start() {
    _start();
  }

  void stop() {
    _stop();
  }

  Stream<Duration> elapsedTimeStream() {
    return _timerStream;
  }
}

extension TimePrinter on Duration {
  static String getDisplayTime(
    Duration duration, {
    bool hours = true,
    bool minute = true,
    bool second = true,
    String hoursRightBreak = ':',
    String minuteRightBreak = ':',
  }) {
    final hoursStr = _getDisplayTimeHours(duration);
    final mStr = _getDisplayTimeMinute(duration);
    final sStr = _getDisplayTimeSecond(duration);
    var result = '';
    if (hours) {
      result += hoursStr;
    }
    if (minute) {
      if (hours) {
        result += hoursRightBreak;
      }
      result += mStr;
    }
    if (second) {
      if (minute) {
        result += minuteRightBreak;
      }
      result += sStr;
    }

    return result;
  }

  static String _getDisplayTimeHours(Duration duration) {
    return (duration.inHours).toString().padLeft(2, '0');
  }

  /// Get display minute time.
  static String _getDisplayTimeMinute(Duration duration) {
    return (duration.inMinutes % 60).toString().padLeft(2, '0');
  }

  /// Get display second time.
  static String _getDisplayTimeSecond(Duration duration) {
    return (duration.inSeconds % 60).toString().padLeft(2, '0');
  }

  String print() {
    return getDisplayTime(this);
  }
}
