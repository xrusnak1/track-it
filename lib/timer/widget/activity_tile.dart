import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/service/utils.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/timer/service/activity_service.dart';
import 'package:track_it/timer/widget/edit_activity.dart';

import '../model/activity.dart';

class ActivityTile extends StatelessWidget {
  final Activity activity;
  final Project? project;

  const ActivityTile(
      {super.key, required this.activity, required this.project});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).colorScheme.background,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(activity.title.shorten(20)), //TODO make this responsive?
              (project != null)
                  ? Row(
                      children: [
                        Icon(Icons.circle, size: 12, color: project!.color),
                        SizedBox(width: 5),
                        Text(project!.name.shorten(16),
                            textAlign: TextAlign.start),
                      ],
                    )
                  : SizedBox.shrink(),
            ],
          ),
          Spacer(),
          Text(Utils.getDurationFormattedString(activity.duration)),
          SizedBox(width: 10),
          IconButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) =>
                          EditActivity(initialActivity: activity)),
                );
              },
              icon: Icon(Icons.edit)),
          IconButton(
              onPressed: () {
                final activityService = GetIt.I.get<ActivityService>();

                activityService.remove(activity.id);
              },
              icon: Icon(Icons.delete)),
        ],
      ),
    );
  }
}
