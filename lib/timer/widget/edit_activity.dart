import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:omni_datetime_picker/omni_datetime_picker.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/timer/service/activity_service.dart';
import 'package:track_it/timer/service/timer_service.dart';

import '../model/activity.dart';

class EditActivity extends StatefulWidget {
  final Activity initialActivity;

  const EditActivity({super.key, required this.initialActivity});

  @override
  State<EditActivity> createState() => _EditActivityState();
}

class _EditActivityState extends State<EditActivity> {
  final TextEditingController _nameController = TextEditingController();
  late Activity
      _modifiedActivity; //for saving the intermediate state without persisting to DB

  @override
  void initState() {
    super.initState();
    _nameController.text = widget.initialActivity.title;
    _modifiedActivity = widget.initialActivity;
  }

  @override
  Widget build(BuildContext context) {
    return FormWrapper(
        appBar: AppBar(
          title: Text('Update activity'),
        ),
        children: [
          FormItem(
              label: 'Title:',
              value: Expanded(
                child: TextField(
                    controller: _nameController,
                    decoration: const InputDecoration(hintText: 'Name')),
              )),
          SizedBox(height: 10),
          FormItem(
            label: 'Duration:',
            value: Text(_modifiedActivity.end
                .difference(_modifiedActivity.begin)
                .print()),
            value2: _buildDateTimePickerButton(),
          ),
          FormItem(label: 'Project:', value: _buildProjectsDropDown()),
          SizedBox(height: 10),
          _buildSaveButton()
        ]);
  }

  Widget _buildSaveButton() {
    return Center(
      child: ElevatedButton(
        // style: ButtonStyle(
        //     minimumSize: MaterialStatePropertyAll<Size?>(Size(10, 50))),
        onPressed: _onActivityUpdatePressed,
        child: Text('Update', style: TextStyle(fontSize: 25)),
      ),
    );
  }

  void _onActivityUpdatePressed() {
    final activityService = GetIt.I.get<ActivityService>();
    activityService.update(_modifiedActivity.copyWith(
      title: _nameController.text,
    ));
    Navigator.pop(context);
  }

  _buildProjectsDropDown() {
    final projectService = GetIt.I.get<ProjectService>();
    final projects = projectService.projectsStream();

    return StreamBuilderWrapper(
      stream: projects,
      builder: (context, projects) {
        final projectId = _modifiedActivity.projectId;
        final projectNotPresent =
            projects.where((project) => project.id == projectId).isEmpty;

        return DropdownButton<String>(
          value: (projectNotPresent) ? null : projectId,
          icon: const Icon(Icons.keyboard_arrow_down),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          items: projects
              .map((e) => DropdownMenuItem<String>(
                    value: e.id,
                    child: Text(e.name),
                  ))
              .toList(),
          onChanged: _onProjectSelectionChanged,
        );
      },
    );
  }

  void _onProjectSelectionChanged(String? newProjectId) {
    setState(() {
      _modifiedActivity = _modifiedActivity.copyWith(projectId: newProjectId);
    });
  }

  _buildDateTimePickerButton() {
    return ElevatedButton(
      onPressed: () async {
        List<DateTime>? newDateTimeList = await showOmniDateTimeRangePicker(
            context: context,
            startInitialDate: _modifiedActivity.begin,
            endInitialDate: _modifiedActivity.end);

        if (newDateTimeList != null && newDateTimeList.length == 2) {
          setState(() {
            try {
              _modifiedActivity = _modifiedActivity.copyWith(
                  begin: newDateTimeList[0], end: newDateTimeList[1]);
            } on Exception catch (e) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(e.toString()),
                ),
              );
            }
          });
        }
      },
      child: Text('Edit'),
    );
  }
}

class FormItem extends StatelessWidget {
  final String label;
  final Widget value;
  final Widget? value2;

  const FormItem(
      {Key? key, required this.label, required this.value, this.value2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(key: UniqueKey(), children: [
      Text(label,
          style: TextStyle(
              fontSize:
                  Theme.of(context).primaryTextTheme.titleLarge?.fontSize)),
      SizedBox(width: 10),
      value,
      SizedBox(width: 10),
      if (value2 != null) value2!
    ]);
  }
}

class FormWrapper extends StatelessWidget {
  const FormWrapper({
    super.key,
    required this.appBar,
    required this.children,
  });

  final AppBar appBar;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Update activity')),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      ),
    );
  }
}
