import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:multiple_stream_builder/multiple_stream_builder.dart';
import 'package:track_it/projects/model/project.dart';
import 'package:track_it/projects/service/project_service.dart';
import 'package:track_it/timer/model/activity.dart';
import 'package:track_it/timer/service/activity_service.dart';
import 'package:track_it/timer/widget/activity_tile.dart';
import 'package:track_it/timer/widget/timer_toolbar.dart';

class TimerPage extends StatelessWidget {
  const TimerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          _buildActivities(),
          TimerToolbar(),
        ],
      ),
    );
  }

  Widget _buildActivities() {
    final activityService = GetIt.I.get<ActivityService>();
    final projectService = GetIt.I.get<ProjectService>();

    return StreamBuilder2<List<Project>, List<Activity>>(
      initialData: InitialDataTuple2(List.empty(), List.empty()),
      streams: StreamTuple2(
          projectService.projectsStream(), activityService.activitiesStream()),
      builder: (BuildContext context, streamSnapshots) {
        if (streamSnapshots.snapshot1.hasError ||
            streamSnapshots.snapshot2.hasError) {
          return Text(
              'Errors: stream1 [${streamSnapshots.snapshot1.error}] stream2 [${streamSnapshots.snapshot2.error}]');
        }

        if (!streamSnapshots.snapshot1.hasData ||
            !streamSnapshots.snapshot2.hasData) {
          return Center(child: CupertinoActivityIndicator());
        }

        final activities = streamSnapshots.snapshot2.data!;
        final projects = streamSnapshots.snapshot1.data!;

        return Expanded(
          child: ListView.separated(
              itemBuilder: (_, i) {
                final activity = activities[i];
                final projectNotPresent = projects
                    .where((project) => project.id == activity.projectId)
                    .isEmpty;
                return ActivityTile(
                  key: UniqueKey(),
                  activity: activity,
                  project: (projectNotPresent)
                      ? null
                      : projects.firstWhere(
                          (project) => project.id == activity.projectId),
                );
              },
              separatorBuilder: (_, __) => SizedBox(height: 10),
              itemCount: activities.length),
        );
      },
    );
  }
}
