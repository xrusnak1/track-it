import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:track_it/common/widget/stream_builder_wrapper.dart';

import '../model/activity.dart';
import '../service/activity_service.dart';
import '../service/timer_service.dart';

class TimerToolbar extends StatefulWidget {
  const TimerToolbar({super.key});

  @override
  State<TimerToolbar> createState() => _TimerToolbarState();
}

class _TimerToolbarState extends State<TimerToolbar> {
  final timerService = GetIt.I.get<TimerService>();
  final activityService = GetIt.I.get<ActivityService>();
  final TextEditingController _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return StreamBuilderWrapper<Duration>(
        stream: timerService.elapsedTimeStream(),
        builder: (BuildContext context, Duration timerValue) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              Expanded(
                child: TextField(
                    controller: _nameController,
                    decoration: const InputDecoration(hintText: 'Name')),
              ),
              SizedBox(width: 8),
              Text(timerValue.print(),
                  style: TextStyle(
                      fontSize: Theme.of(context)
                          .primaryTextTheme
                          .headlineSmall
                          ?.fontSize)),
              SizedBox(width: 10),
              _buildCircleButton(
                context,
                onPressed: () {
                  timerService.isRunning ? _stopActivity() : _startActivity();
                },
                icon: timerService.isRunning
                    ? Icon(Icons.stop_circle)
                    : Icon(Icons.play_circle),
              ),
            ]),
          );
        });
  }

  _startActivity() {
    timerService.start();
  }

  _stopActivity() {
    timerService.stop();
    _saveNewActivity();
  }

  _saveNewActivity() {
    if (!timerService.isRunning) {
      activityService.add(
        Activity.generatedId(
          timerService.beginDateTime!,
          timerService.endDateTime!,
          _nameController.text.isEmpty ? 'New activity' : _nameController.text,
          null,
          null,
        ),
      );
      _nameController.clear();
    }
  }

  CircleAvatar _buildCircleButton(BuildContext context,
      {required void Function() onPressed,
      required Icon icon,
      bool enabled = true}) {
    return CircleAvatar(
      radius: 30,
      backgroundColor: Theme.of(context).colorScheme.background,
      child: IconButton(
        iconSize: 40,
        onPressed: enabled ? onPressed : null,
        icon: icon,
      ),
    );
  }
}
